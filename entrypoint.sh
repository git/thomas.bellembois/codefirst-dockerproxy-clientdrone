#!/usr/bin/env bash
ProxyScheme=""
ProxyHost=""
ProxyPath=""

ImageName=""
ContainerName=""
Overwrite=""
Private=""
Env=""
Command=""
Admins=""

if [ ! -z "$PROXYSCHEME" ]
then
    ProxyScheme="-proxyscheme $PROXYSCHEME"
fi

if [ ! -z "$PROXYHOST" ]
then
    ProxyHost="-proxyhost $PROXYHOST"
fi

if [ ! -z "$PROXYPATH" ]
then
    ProxyPath="-proxypath $PROXYPATH"
fi

if [ ! -z "$IMAGENAME" ]
then
    ImageName="-imagename $IMAGENAME"
fi

if [ ! -z "$CONTAINERNAME" ]
then
    ContainerName="-containername $CONTAINERNAME"
fi

if [ ! -z "$COMMAND" ]
then
    Command="-command $COMMAND"
fi

if [ ! -z "$ADMINS" ]
then
    Admins="-admins $ADMINS"
fi


if [ ! -z "$PRIVATE" ]
then
    Private="-private"
fi

if [ ! -z "$OVERWRITE" ]
then
    Overwrite="-overwrite"
fi

prefix="CODEFIRST_CLIENTDRONE_ENV_"
ENVS=$(env | awk -F "=" '{print $1}' | grep ".*$prefix.*")

if [ ! -z "$ENVS" ]
then
    Env=""
    arrayEnv=($ENVS)

    for i in "${arrayEnv[@]}"
    do
        envVarName=${i#"$prefix"}
        Env=$Env" -env $envVarName=${!i}"
    done
fi

echo $ProxyScheme
echo $ProxyHost
echo $ProxyPath

echo $ImageName
echo $ContainerName
echo $Overwrite
echo $Private
echo $Admins
echo $Env
echo $Command

#/go/bin
sh -c "/go/bin/codefirst-dockerproxy-clientdrone $ProxyScheme $ProxyHost $ProxyPath $ImageName $ContainerName $Private $Admins $Overwrite $Env $Command"
